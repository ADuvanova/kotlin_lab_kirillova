package com.example.myapplication

import kotlin.random.Random

class IntSum {

    var numbers: List<Int> = listOf()
    var sumInt: Int = 0

    init {
        updateNumbers()
    }

    fun updateNumbers() {
        numbers = List(10) { Random.nextInt(1, 20) }
    }

    fun IntSumOut(): Int {
        for (n in numbers) {
            if (n % 1 == 0 && n % n == 0)
                sumInt += n

        }
        return sumInt
    }
}


